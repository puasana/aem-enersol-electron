# AEM Enersol Electron

This project is created as one of two assessments from AEM Enersol.

## How to set up this project

Clone this project from gitlab:
`git clone https://gitlab.com/puasana/aem-enersol-electron.git`

Install NPM Package:
`npm install`

Run Client:
`npm start`

Build and Run Electron:
`npm run electron`
