import { Component, OnInit, Inject, NgZone, PLATFORM_ID, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { DashboardService } from 'src/app/services/dashboard.service';
import { isPlatformBrowser } from '@angular/common';

import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {
  private pieChart?: am4charts.PieChart;
  private barChart?: am4charts.XYChart;

  constructor(
    @Inject(PLATFORM_ID) private platformId: any, 
    private zone: NgZone,
    private authService: AuthService,
    private dashboardService: DashboardService 
  ){}

  ngOnInit() {
    this.authService.isLoggedIn();
  }

  browserOnly(f: () => void) {
    if (isPlatformBrowser(this.platformId)) {
      this.zone.runOutsideAngular(() => {
        f();
      });
    }
  }

  ngAfterViewInit() {
    const token = this.authService.getToken();
    if (token) {
      this.showDashboard(token);
    }
  }

  ngOnDestroy() {
    this.browserOnly(() => {
      if (this.pieChart) {
        this.pieChart.dispose();
      }

      if (this.barChart) {
        this.barChart.dispose();
      }
    });
  }

  users: any = [];

  showDashboard(token: string) {
    this.dashboardService.getData(token).subscribe((data: any) => {
      this.users = data.tableUsers;
      this.showPieChart(data.chartDonut);
      this.showBarChart(data.chartBar);

      console.log(this.users);
    })
  }

  showPieChart(data: any) {
    this.browserOnly(() => {
      am4core.useTheme(am4themes_animated);

      let pieChart = am4core.create("pieChart", am4charts.PieChart);

      pieChart.paddingLeft = 100;
      pieChart.paddingRight = 100;
      pieChart.innerRadius = am4core.percent(40);

      pieChart.data = data;

      let series = pieChart.series.push(new am4charts.PieSeries());
      series.dataFields.value = "value";
      series.dataFields.category = "name";

      this.pieChart = pieChart;
    });
  }

  showBarChart(data: any) {
    this.browserOnly(() => {
      am4core.useTheme(am4themes_animated);

      let barChart = am4core.create("barChart", am4charts.XYChart);

      barChart.data = data;

      let categoryAxis = barChart.xAxes.push(new am4charts.CategoryAxis());
      categoryAxis.dataFields.category = "name";
      categoryAxis.title.text = "Name";

      let valueAxis = barChart.yAxes.push(new am4charts.ValueAxis());
      valueAxis.title.text = "Value";

      let series = barChart.series.push(new am4charts.ColumnSeries());

      series.name = "Bar Chart";
      series.columns.template.tooltipText = "Name: {categoryX}\nValue: {valueY}";
      series.columns.template.fill = am4core.color("#104547"); 
      series.dataFields.valueY = "value";
      series.dataFields.categoryX = "name";
      series.stacked = true;

      this.barChart = barChart;
    });
  }

  signOut() {
	  this.authService.signOut();
  }
}


