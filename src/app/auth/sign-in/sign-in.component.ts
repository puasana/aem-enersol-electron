import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SignInService } from 'src/app/services/sign-in.service';
import { PouchDBService } from 'src/app/services/pouchdb.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.sass']
})
export class SignInComponent implements OnInit {
  username!: FormControl;
  password!: FormControl;

  signInFormGroup!: FormGroup;

  loadingMessage: string = '';
  errorMessage: string = '';

  constructor(
    private form: FormBuilder,
    private signInService: SignInService, 
    private authService: AuthService, 
    private pouchDBService: PouchDBService,
    private router: Router,
  ) {}

  ngOnInit() {
    this.createFormControls();
    this.createFormGroup();

    this.authService.isLoggedIn();
  }

  createFormControls() {
    this.username = new FormControl('', [Validators.required]);
    this.password = new FormControl('', [Validators.required]);
  }

  createFormGroup() {
    this.signInFormGroup = this.form.group({
      username: this.username,
      password: this.password
    });
  }

  onSignIn() {
    this.hideError();
    this.showLoading();

    if (this.signInFormGroup.valid) {
      this.signInService.checkCredential(this.signInFormGroup.value).subscribe((data: any) => {
        // console.log('data=>', data);
        const token = data;
        this.authService.storeToken(token);
        this.storeTokenOnPouchDB(this.signInFormGroup.get('username')!.value, this.signInFormGroup.get('password')!.value, token);
  
        setTimeout(() => {
          this.hideLoading();
          this.router.navigate(['']);
        }, 500);
      }, (error) => {
        setTimeout(() => {
          this.checkPouchDB(this.signInFormGroup.get('username')!.value, this.signInFormGroup.get('password')!.value);
          this.hideLoading();
          this.showCredentialError();
        }, 500);
      });
    } else {
      setTimeout(() => {
        this.checkPouchDB(this.signInFormGroup.get('username')!.value, this.signInFormGroup.get('password')!.value);
        this.showError();
        this.hideLoading();
      }, 500);
    }
  }

  storeTokenOnPouchDB(username: string, password: string, token: string){
    this.pouchDBService.store(username, password, token);
  }

  checkPouchDB(username: any, password: any) {
    this.pouchDBService.checkCredential(username, password);
  }

  showLoading() {
    this.loadingMessage = 'Checking credential. Please wait...';
    return this.loadingMessage;
  }

  hideLoading() {
    this.loadingMessage = '';
    return this.loadingMessage;
  }

  showError() {
    this.errorMessage = 'Please enter your credential correctly.';
    return this.errorMessage;
  }

  hideError() {
    this.errorMessage = '';
    return this.errorMessage;
  }

  showCredentialError() {
    this.errorMessage = 'Your credential is wrong. Please try again.';
    return this.errorMessage;
  }
}

