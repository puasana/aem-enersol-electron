import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService { 
  constructor(
    private router: Router
  ) {}

  isLoggedIn() {
    const token = this.getToken();

    if (token == null) {
      this.router.navigate(['sign-in']);
    } else {
      this.router.navigate(['']);
    }
  }

  getToken() {
    return localStorage.getItem('app_token');
  }

  getExpirationToken() {
    return localStorage.getItem('app_token');
  }

  storeToken(value: string) {
    return localStorage.setItem('app_token', value);
  }

  deleteToken() {
    return localStorage.removeItem('app_token');
  }

  signOut() {
    this.deleteToken();
    this.router.navigate(['sign-in']);
  }
}
