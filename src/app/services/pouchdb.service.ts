import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import PouchDB from 'pouchdb';

@Injectable({
  providedIn: 'root'
})
export class PouchDBService {
  pouchdb: any;

  constructor(
    private router: Router
  ) { 
    this.pouchdb = new PouchDB("aem_enersol_electron_db"); 
  }

  store(username: string, password: string, token: string) {
    this.pouchdb.put({
      _id: new Date().toISOString(),
      username: username,
      password: password,
      token: token
    }).then(function (response: any) {
      // handle response
    }).catch(function (err: any) {
      console.log(err);
    });
  }

  checkCredential(username: string, password: string) {
    this.pouchdb.allDocs({
      include_docs: true,
      attachments: true
    }).then( (result: any) => {
      if (result.rows.length == 0) {
        this.router.navigate(['']);
      }
    }).catch(function (err: any) {
      console.log(err);
    });
  }
}
